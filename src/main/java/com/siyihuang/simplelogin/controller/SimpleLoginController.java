package com.siyihuang.simplelogin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class SimpleLoginController {
    @GetMapping("/test")
    public String loginTest() {
        return "test login success";
    }
}
