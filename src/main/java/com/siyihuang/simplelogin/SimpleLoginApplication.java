package com.siyihuang.simplelogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

// 暂时屏蔽mybatis数据库
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SimpleLoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleLoginApplication.class, args);
    }

}
